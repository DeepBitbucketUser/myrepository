
from tensorflow.python.keras.applications.inception_v3 import InceptionV3
from tensorflow.python.keras.optimizers import SGD
from tensorflow.python.keras.layers import Dense, Dropout


class DNNmodel(object):

    def __init__(self):

        self.save_period = 1
        self.freeze_first_n_layers = 249
        self.model_shape = 299

        self.use_model_shape = False

        self.train_split = 'train_split.txt'
        self.test_split = 'test_split.txt'
        self.predict_split = 'test_split.txt'

        self.batch_size = 1
        self.lr = 0.0003
        self.decay = 0.0

    def get_name(self):
        return 'TInceptionV3_gap'

    def get_output_layer_name(self):
        # return 'avg_pool'
        return None

    def get_data_split(self):
        return {
            'train': self.train_split,
            'test': self.test_split,
            'predict': self.predict_split
        }

    def get_generator(self):
        return None



    def get_base_model(self, include_top=False):
        return InceptionV3(weights='imagenet', include_top=include_top)

    def get_retrain_parameters(self):
        return {
            'steps_per_epoch': 140000,
            'epochs': 40,
            'validation_steps': 17000,
        }


    def get_optimizer(self):
        return SGD(
            lr = self.lr
        )

    def create_dense(self, x):

        # x = Dropout(0.6, input_shape=(2048,))(x)
        # x = Dense(1024, activation='relu')(x)
        # x = Dense(512, activation='relu')(x)
        x = Dense(256, activation='relu')(x)
        # x = Dense(64, activation='relu')(x)

        return x


