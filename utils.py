
import numpy as np
import cv2


def normalize_image(image):
    image = np.array(image, dtype=np.float32)
    image /= 127.5
    image -= 1.
    return image

def denormalize_image(image):
    image += 1.
    image *= 127.5
    image = np.clip(image, 0, 255)
    return np.array(image, dtype=np.uint8)


def process_pil_image(image):

    MODEL_SHAPE = 299

    assert image.mode == 'RGB', image.mode


    width = image.width
    height = image.height

    if height < width:
        koef = height / float(MODEL_SHAPE)
        new_width = int(width / koef)
        new_height = MODEL_SHAPE
    else:
        koef = width / float(MODEL_SHAPE)
        new_height = int(height / koef)
        new_width = MODEL_SHAPE

    # size: 2-tuple (width, height)
    image = image.resize((new_width, new_height))

    im = np.array(image, dtype=np.float32)
    # RGB --> BGR
    im = im[:,:,::-1]
    im = normalize_image(im)

    assert np.ndim(im) == 3, im.shape

    return im

def process_cv2_image(image, model_shape=299):

    MODEL_SHAPE = model_shape

    height, width, _ = image.shape

    if height < width:
        koef = height / float(MODEL_SHAPE)
        new_width = int(width / koef)
        new_height = MODEL_SHAPE
    else:
        koef = width / float(MODEL_SHAPE)
        new_height = int(height / koef)
        new_width = MODEL_SHAPE

    image = cv2.resize(src=image, dsize=(new_width, new_height))
    # image = normalize_image(np.array(image, dtype=np.float32))

    assert np.ndim(image) == 3, 'Crop_to_min_shape ndims assert'

    return image