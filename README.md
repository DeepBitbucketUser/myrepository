
### Main requirements:
    tensorflow 1.4 or higher


### Data folder hierarchy:
	data/
		0/
			*.jpg
		1/
			*.jpg


		27/
			*.jpg

### First run:
    python create_json_and_split.py --image_dir ./data/
to create json files containing class info and train/test split.


### Train/test/predict example runs:
    train:
        bash run_train.sh 3 vgg16 data/
    retrain existing model:
         bash run_retrain.sh 3 vgg16 data/ models_keras/VGG16/0/weights-improvement-32.hdf5

    test:
         bash run_test.sh 1 resnet50 data/ models_keras/ResNet50/0/weights-improvement-10.hdf5
    predict:
        bash run_test.sh 1 vgg19_gap data/ models_keras/TVGG19_gap/1/weights-improvement-06.hdf5
