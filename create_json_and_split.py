
import argparse
import os
import glob
import json
import random

from cls_info import get_cls_string


def main(args):

    assert os.path.exists(args.image_dir), args.image_dir

    image_classes_dirs = os.listdir(args.image_dir)
    print(f'Found image classes: {image_classes_dirs}')
    classes = [int(x) for x in image_classes_dirs]
    image_names = []

    for cls in classes:
        cls_string = get_cls_string(cls)
        print(f'cls string: {cls_string}')
        image_cls_dir = os.path.join(args.image_dir, str(cls))
        image_paths = glob.glob(os.path.join(image_cls_dir, '*.jpg'))

        for image_path in image_paths:
            basename = os.path.basename(image_path).split('.')[0]
            print(f'Base name: {basename}')
            image_names.append(basename)
            json_path = os.path.join(image_cls_dir, basename + '.annotation.json')

            json_data = {'class': cls_string}

            with open(json_path, 'w') as outfile:
                json.dump(json_data, outfile)

    # shuffle
    random.shuffle(image_names)

    break_point = int(args.train_percent * len(image_names))
    train_split = image_names[:break_point]
    test_split = image_names[break_point:]

    train_name = 'train_' + args.split_name + '.txt'
    test_name = 'test_' + args.split_name + '.txt'

    # with open('image_names.txt', 'w') as f:
    #     f.write('\n'.join(image_names))

    with open(train_name, 'w') as f:
        f.write('\n'.join(train_split))
    with open(test_name, 'w') as f:
        f.write('\n'.join(test_split))



if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--image_dir',
                        type=str,
                        default='data'
                        )
    parser.add_argument('--split_name',
                        type=str,
                        default='split'
                        )
    parser.add_argument('--train_percent',
                        type=float,
                        default=0.8
                        )


    args = parser.parse_args()
    main(args=args)