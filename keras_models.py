import os
# os.environ["CUDA_VISIBLE_DEVICES"] = "3"


import tensorflow as tf

from tensorflow.python.keras.models import Model
from tensorflow.python.keras.layers import Dense, GlobalAveragePooling2D
from tensorflow.python.keras.callbacks import ModelCheckpoint, Callback, LearningRateScheduler, TensorBoard
from tensorflow.python.keras._impl.keras.backend import set_session, get_session
from tensorflow.python.keras import metrics

from cls_info import get_cls_ind, get_cls_string
import cv2
import numpy as np
import json, copy
import random
import argparse
import importlib
import glob, csv
import datetime
from functools import partial
from sklearn.metrics import accuracy_score


FLAGS = None

BASE_MODEL_SAVE_PATH = 'models_keras'
SAVE_OUTPUTS_DIR = 'outputs'
NUM_CLASSES = 27


def crop_to_min_shape(image, use_model_shape, model_shape):

    height, width, _ = image.shape

    if use_model_shape:
        min_shape = min([height, width])
        image = image[0:min_shape, 0:min_shape]
        image = cv2.resize(src=image, dsize=(model_shape, model_shape))
    else:
        # print(f'h, w = {height}, {width} || model: {model_shape}')
        if height < width:
            koef = height / float(model_shape)
            new_width = int(width / koef)
            new_height = model_shape
        else:
            koef = width / float(model_shape)
            new_height = int(height / koef)
            new_width = model_shape
        # print(f'nh, nw = {new_height}, {new_width} || model: {model_shape}')

        # dsize = (width, height)
        image = cv2.resize(src=image, dsize=(new_width, new_height))

    assert np.ndim(image) == 3, 'Crop_to_min_shape ndims assert'

    return image

def normalize_image(image):
    image = np.array(image, dtype=np.float32)
    image /= 127.5
    image -= 1.

    return image


def generator_wrap(keras_model, mode, one_run_flag=False):

    data_split = keras_model.get_data_split()
    use_model_shape = keras_model.use_model_shape
    model_shape = keras_model.model_shape

    split = data_split[mode]

    def generator(image_dir, split):
        assert os.path.exists(image_dir), image_dir
        while True:
            with open(split, 'r') as f:
                image_names = f.readlines()

                print(f'Found {len(image_names)} names')

                # shuffle
                random.shuffle(image_names)


            for i, image_name in enumerate(image_names):

                image_name = image_name.strip()
                image_path_glob = os.path.join(image_dir, '*/' + image_name + '.jpg')
                # image_path_glob = image_dir + '/' + image_name + '.jpg'
                image_path = glob.glob(image_path_glob)
                assert len(image_path) == 1, f'Image name: {image_name}, image_path: {image_path}'

                image_path = image_path[0]
                assert os.path.exists(image_path), image_path

                # expects 3 channel input
                image = cv2.imread(image_path)

                if image is None:
                    pass
                else:
                    base = os.path.dirname(image_path)
                    json_path = os.path.join(base, image_name + '.annotation.json')

                    cls = 0
                    if FLAGS.phase != 'predict':
                        assert os.path.exists(json_path), f'No json file: {json_path}, with base: {base}'

                        with open(json_path, 'r') as f:
                            obj = json.load(f)

                        cls = get_cls_ind(obj['class'])
                        cls = int(cls)

                        if cls != 0 and FLAGS.phase != 'test':

                            im = copy.deepcopy(image)
                            p = random.uniform(0., 1.)

                            if p <= 0.3:
                                im = cv2.flip(im, 0)
                            elif p > 0.3 and p < 0.5:
                                im = cv2.flip(im, 1)
                            elif p > 0.5 and p < 0.8:
                                im = cv2.transpose(im)
                                im = cv2.flip(im, +1)
                            else:
                                im = cv2.flip(im, 0)
                                im = cv2.flip(im, 1)

                            im = crop_to_min_shape(im, use_model_shape, model_shape)
                            im = normalize_image(im)

                            cls_null = np.zeros((1, NUM_CLASSES))
                            cls_null[0][0] = 1
                            im = np.expand_dims(im, axis=0)

                            assert im is not None and np.ndim(im) == 4
                            assert cls_null is not None

                            yield (im, cls_null)


                    image = crop_to_min_shape(image, use_model_shape, model_shape)
                    image = normalize_image(image)

                    image = np.expand_dims(image, axis=0)

                    assert image.size > 0

                    cls_vector = np.zeros((1, NUM_CLASSES))
                    cls_vector[0][cls] = 1

                    assert cls_vector.size > 0

                    assert image is not None and np.ndim(image) == 4
                    assert cls_vector is not None


                    if FLAGS.phase in {'predict', 'test'}:
                        yield (image, cls_vector, image_name)
                    else:
                        yield (image, cls_vector)

            if one_run_flag:
                raise StopIteration

    return generator(FLAGS.image_dir, split)



def batch_generator(keras_model, mode='train', one_run_flag=False):

    assert mode in ['train', 'test']
    batch_size = keras_model.batch_size
    if mode == 'train':
        cls_stats = np.zeros(NUM_CLASSES)
        cls_stats_name = os.path.join(BASE_MODEL_SAVE_PATH, 'cls_stats_' + keras_model.get_name().strip() + '.txt')
        if os.path.exists(cls_stats_name):
            os.remove(cls_stats_name)
        print(f'Using stats name: {cls_stats_name}')
        cls_log_every_n = 35000  # log every n examples


    while 1:
        features = []
        labels = []
        count = 0
        _JUNK_REPEAT = 26
        last_seen_zero_cls = 0
        global_count = 0


        for feature, label in generator_wrap(keras_model, mode, one_run_flag):
            if count < batch_size:
                if (label[0][0] == 1.0 and last_seen_zero_cls >= _JUNK_REPEAT) or label[0][0] != 1.0:

                    features.append(feature)
                    labels.append(label)
                    count += 1
                    global_count += 1
                    if label[0][0] == 1.0:
                        last_seen_zero_cls = 0
                    else:
                        last_seen_zero_cls += 1

                    if mode == 'train':
                        cls_stats[np.argmax(label[0])] += 1
                        cls_out = [(x / float(global_count)) * 100 for x in cls_stats]
                        if global_count % cls_log_every_n == 0:
                            with open(cls_stats_name, 'a') as f:
                                f.write(' '.join(['%0.2f' % x for x in cls_out]) + '\n')

            else:
                features = np.concatenate(features, axis=0)
                labels = np.concatenate(labels, axis=0)
                yield (np.array(features), np.array(labels))
                features = []
                labels = []
                count = 0



class TrainLossLogger(Callback):
    def __init__(self, display):
        self.seen = 0
        self.display = display

    def on_batch_end(self, batch, logs={}):
        self.seen += 1
        if self.seen % self.display == 0:
            loss = logs.get('loss', 0)
            # iter = int(self.seen / self.display)

            # print(f'seen: {self.seen}, batch: {batch}, logs: {logs}')
            print(f'\nIter: {self.seen}, loss = {loss}\n')




def exp_decay(epoch, initial_lrate):
    k = 0.1
    lrate = initial_lrate * np.exp(-2 * k * epoch)
    return lrate


def create_train_callbacks_list(keras_model):

    base_model_dir = os.path.join(BASE_MODEL_SAVE_PATH, keras_model.get_name())
    time = str(datetime.datetime.now())
    model_dir = os.path.join(base_model_dir, time)

    if not os.path.exists(model_dir):
        os.mkdir(model_dir)

    checkpoint_filepath = os.path.join(model_dir, "weights-improvement-{epoch:02d}.hdf5")
    checkpoint = ModelCheckpoint(
        filepath=checkpoint_filepath,
        monitor='loss',
        period=keras_model.save_period
    )

    return [checkpoint,
            TensorBoard(log_dir=model_dir,
                        write_graph=False,
                        write_images=True),
            # TrainLossLogger(keras_model.log_every_n_steps),
            LearningRateScheduler(partial(exp_decay,
                                          initial_lrate=keras_model.lr))
            ]


def get_base_and_prediction(keras_model):

    base_model = keras_model.get_base_model(include_top=True) if keras_model.use_model_shape else \
        keras_model.get_base_model(include_top=False)


    if not keras_model.use_model_shape:
        # add a global spatial average pooling layer
        x = GlobalAveragePooling2D()(base_model.output)
    else:
        output_layer_name = keras_model.get_output_layer_name()
        assert output_layer_name is not None
        x = base_model.get_layer(output_layer_name).output

    if keras_model.get_name() == 'TVGG16_gap':
        print('TVGG16_gap dense ....')
        x = keras_model.create_dense(x)
    elif keras_model.get_name() == 'TVGG19_gap':
        print('TVGG19_gap dense ....')
        x = keras_model.create_dense(x)
    elif keras_model.get_name() == 'TResNet50_gap':
        print('TResNet50_gap dense ....')
        x = keras_model.create_dense(x)
    elif keras_model.get_name() == 'TInceptionV3_gap':
        print('TInceptionV3_gap dense ....')
        x = keras_model.create_dense(x)
    else:
        # let's add a fully-connected layer
        x = Dense(1024, activation='relu')(x)
        x = Dense(512, activation='relu')(x)
        x = Dense(256, activation='relu')(x)

    predictions = Dense(NUM_CLASSES, activation='softmax')(x)

    return base_model, predictions



def test_model(keras_model, weights_file):

    assert os.path.exists(weights_file), weights_file

    base_model, predictions = get_base_and_prediction(keras_model)
    model = Model(inputs=base_model.input, outputs=predictions)

    model.load_weights(weights_file)
    print("Model loaded")

    generator = generator_wrap(keras_model,
                               mode=FLAGS.phase,
                               one_run_flag=True)

    # cummulate predictions
    cls_prediction = []
    cls_gt = []

    if FLAGS.phase == 'predict':
        base = keras_model.get_data_split()['predict']
        base = os.path.basename(base)
        base = base.split('.')[0]

        csv_path = os.path.join(SAVE_OUTPUTS_DIR, base)
        if not os.path.exists(csv_path):
            os.mkdir(csv_path)
            print(f'Created directory: {csv_path}')

        csv_file = os.path.join(csv_path, keras_model.get_name() + '.csv')
        f = open(csv_file, 'w', newline='')
        csv_writer = csv.writer(f, delimiter=',')


    for counter, (image, label, hash) in enumerate(generator):
        prediction = model.predict(image, 1, verbose=0)
        cls_prediction.append(np.argmax(prediction))
        cls_gt.append(np.argmax(label[0]))


        if FLAGS.phase == 'predict':
            cls_string = get_cls_string(np.argmax(prediction))
            csv_writer.writerow([hash, cls_string])

        if (counter+1) % 10000 == 0:
            print(f'Done: {counter}')

    score = accuracy_score(cls_gt, cls_prediction)

    print("Accuracy: {}".format(score))

    if FLAGS.phase == 'predict':
        print(f'Csv saved to path: {csv_file}')
        f.close()





def retrain_model(keras_model, weights=None):


    base_model, predictions = get_base_and_prediction(keras_model)

    # this is the model we will train
    model = Model(inputs=base_model.input, outputs=predictions)

    if weights is not None:
        print(f'Loading weights: {weights}')
        model.load_weights(weights)


    for layer in base_model.layers:
        layer.trainable = False


    model.compile(
        optimizer=keras_model.get_optimizer(),
        loss='categorical_crossentropy',
        metrics=[metrics.categorical_accuracy]
    )

    model.fit_generator(
        generator=batch_generator(keras_model, one_run_flag=True),
        validation_data=batch_generator(keras_model, mode='test', one_run_flag=True),
        callbacks=create_train_callbacks_list(keras_model),
        **keras_model.get_retrain_parameters(),
    )


    if keras_model.freeze_first_n_layers is not None:
        for layer in model.layers[:keras_model.freeze_first_n_layers]:
            layer.trainable = False
        for layer in model.layers[keras_model.freeze_first_n_layers:]:
            layer.trainable = True



    # compile the model (should be done *after* setting layers to non-trainable)
    model.compile(
        optimizer=keras_model.get_optimizer(),
        loss='categorical_crossentropy',
        metrics=[metrics.categorical_accuracy]
    )

    # train the model on the new data for a few epochs
    model.fit_generator(
        generator=batch_generator(keras_model),
        validation_data=batch_generator(keras_model, mode='test'),
        callbacks=create_train_callbacks_list(keras_model),
        **keras_model.get_retrain_parameters(),
    )



def inspect_model(keras_model):

    base_model, predictions = get_base_and_prediction(keras_model)
    model = Model(inputs=base_model.input, outputs=predictions)

    if FLAGS.inspect_tag == 0:
        for i, layer in enumerate(base_model.layers):
            print(i, layer.name, layer.output)

    elif FLAGS.inspect_tag == 1:
        for i, layer in enumerate(model.layers):
            print(i, layer.name, layer.output)

    else:
        print('Unsupported inspection tag')
        exit(1)



def export_model(keras_model, weights_file):

    base_model, predictions = get_base_and_prediction(keras_model)
    model = Model(inputs=base_model.input, outputs=predictions)

    model.load_weights(weights_file)
    print('Model loaded')

    # extract global step form: weig..-impro...-XX.hdf5
    base_name = os.path.basename(weights_file)
    weights_name = base_name.split('.')[0]
    try:
        global_step = int(weights_name[-2:])
    except ValueError:
        print('Int expected got: {}, from weights file: {}'.format(weights_name[-2:], weights_file))
        exit(1)


    dir_path = os.path.dirname(weights_file)
    ckpt_path = os.path.join(dir_path, keras_model.get_name() + '_model.ckpt')


    sess = get_session()
    saver = tf.train.Saver()

    # save checkpoint
    saver.save(sess=sess,
               save_path=ckpt_path,
               global_step=global_step)

    # save graph
    writer = tf.summary.FileWriter(dir_path, sess.graph)
    writer.close()

    print('Model exported')




def main(_):

    assert FLAGS.gpu
    assert FLAGS.model_name
    assert FLAGS.phase
    assert FLAGS.phase in ['train', 'test', 'predict', 'inspect', 'export']
    assert os.path.exists(FLAGS.image_dir), FLAGS.image_dir

    print(f'Doing {FLAGS.phase} phase.')

    import_name = 'keras_' + FLAGS.model_name
    print(f'Import name: {import_name}')
    model_lib = importlib.import_module(import_name)
    keras_model = model_lib.DNNmodel()

    model_name = keras_model.get_name()
    dir_name = os.path.join(BASE_MODEL_SAVE_PATH, model_name)
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)


    def get_session():
        gpu_options = tf.GPUOptions(allow_growth=True,
                                    per_process_gpu_memory_fraction = 0.5
                                    )
        return tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))

    # set session
    set_session(get_session())

    if FLAGS.phase == 'train':
        retrain_model(keras_model, FLAGS.weights)
    elif FLAGS.phase == 'inspect':
        inspect_model(keras_model)

    else:
        assert FLAGS.weights
        assert os.path.exists(FLAGS.weights), FLAGS.weights

        if FLAGS.phase == 'export':
            export_model(keras_model, FLAGS.weights)

        else:
            test_model(keras_model,
                       FLAGS.weights
                       )




if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--gpu',
        type=str,
        default=0
    )
    parser.add_argument(
        '--model_name',
        type=str
    )
    parser.add_argument(
        '--image_dir',
        type=str,
        default=''
    )
    parser.add_argument(
        '--phase',
        type=str
    )
    parser.add_argument(
        '--weights',
        type=str,
        default=None
    )
    parser.add_argument(
        '--inspect_tag',
        type=int,
        help='0 inspect base, 1 inspect predict'
    )


    FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main=main)
        
