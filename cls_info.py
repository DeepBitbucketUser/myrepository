# -*- coding: utf-8 -*-
import os
import os.path as osp
import numpy as np
import operator
# `pip install easydict` if you don't have it
from easydict import EasyDict as edict
__C = edict()
# Consumers can get config by:
#   from fast_rcnn_config import cfg
chars = __C
_class_dict = { 'junk'              : "junk",
                "anybar"            : "junk",
                "baker's"           : "junk",
                "citymarket"       : "junk",
                "dillon's"          : "junk",
                "food4less"         : "junk",
                "foodsco"          : "junk",
                "shaw's"            : "junk",
                "sprouts"           : "junk",
                "starmarket"       : "junk",
                'acme'              : "junk",
                "hy-vee"            : "hy-vee",
                "bjs"               : "bjs",
                "bj's"               : "bjs",
                "harristeeter"      : "harris_teeter",
                "kingsoopers"       : "king-soopers",
                "albertsons"        : "albertsons",
                "winn-dixie"        : "winn_dixie",
                "smiths"            : "smiths",
                "smith's"            : "smiths",
                "frys"              : "frys",
                "fry's"              : "frys",
                "samsclub"          : "sams_club",
                "sam'sclub"          : "sams_club",
                "jewel-osco"        : "jewel_osco",
                "wincofoods"        : "win_co_foods",
    'shoprite'          : "shop_rite",
    'kroger'            : "kroger",
    'stopshop'          : "stop_shop",
    'stop&shop'          : "stop_shop",
    'wegmans'           : "wegmans",
    'safeway'           : "safeway",
    'meijer'            : "meijer",
    'h-e-b'             : "h-e-b",
    'publix'            : "publix",
    'cvspharmacy'       : "cvs_pharmacy",
    'cvs'               : "cvs_pharmacy",
                'walmart'           : "walmart",
                'costco'            : 'costco',
                'fredmeyer'        : 'fred_meyer',
                'target'            : 'target',
                'walgreens'         : 'walgreens',
                'wholefoodsmarket': 'whole_foods',
                'wholefoodsmarketå¨': 'whole_foods',
                }
_cls_inds_dict = {"junk": 0,
                  "costco":1,
                  "fred_meyer":2,
                  "target":3,
                  "walgreens":4,
                  "whole_foods":5,
                  "walmart":6,
                  "shop_rite":7,
                  "kroger":8,
                  "stop_shop":9,
                  "wegmans":10,
                  "safeway":11,
                  "meijer":12,
                  "h-e-b":13,
                  "publix":14,
                  "cvs_pharmacy":15,
                  "hy-vee":16,
                  "bjs":17,
                  "harris_teeter":18,
                  "king-soopers":19,
                  "albertsons":20,
                  "winn_dixie":21,
                  "smiths":22,
                  "frys":23,
                  "sams_club":24,
                  "jewel_osco":25,
                  "win_co_foods":26
                  }

_cls_inds_dict_inv = {v: k for k, v in _cls_inds_dict.items()}


def get_model_classes():
    sorted_classes = sorted(_cls_inds_dict.iteritems(), key=operator.itemgetter(1))
    sorted_classes = [x[0].decode("UTF-8") for x in sorted_classes]
    return sorted_classes
def get_replacement(cls):
    return _class_dict[cls]
def get_cls_ind(cls):
    cls = cls.replace(" ", "")
    cls = cls.lower()
    if "wholefoodsmarket" in cls:
        cls = "wholefoodsmarket"
    if cls in _class_dict:
        name = _class_dict[cls]
        return _cls_inds_dict[name]
    else:
        # print(cls)
        return _cls_inds_dict["junk"]

def get_cls_string(index):
    return _cls_inds_dict_inv[index]


if __name__ == '__main__':
    print()