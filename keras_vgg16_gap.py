from tensorflow.python.keras.applications.vgg16 import VGG16
from tensorflow.python.keras.optimizers import SGD, Adam
from tensorflow.python.keras.layers import Dense



class DNNmodel(object):

    def __init__(self):

        # split
        self.train_split = 'train_split.txt'
        self.test_split = 'test_split.txt'
        self.predict_split = 'test_split.txt'


        # input image shapes
        self.use_model_shape = False
        self.model_shape = 224
        self.min_shape = 48

        self.save_period = 1 # Interval (number of epochs) between checkpoints.
        self.freeze_first_n_layers = None
        self.batch_size = 1

        self.log_every_n_steps = 250


        self.lr = 0.0003
        self.decay = 0.0


    def get_name(self):
        return 'TVGG16_gap'

    def get_output_layer_name(self):
        # return 'fc2'
        return None

    def get_data_split(self):
        return {
            'train': self.train_split,
            'test': self.test_split,
            'predict': self.predict_split
        }

    def get_generator(self):
        return None


    def get_base_model(self, include_top=False):
        return VGG16(weights='imagenet', include_top=include_top)


    def get_retrain_parameters(self):
        return {
            'steps_per_epoch': 140000,
            'epochs': 40,
            'validation_steps': 17000,
        }


    def get_optimizer(self):
        return SGD(
            lr = self.lr
        )

    def create_dense(self, x):
        
        x = Dense(256, activation='relu')(x)
        x = Dense(128, activation='relu')(x)
        x = Dense(64, activation='relu')(x)
        
        return x

