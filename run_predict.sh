#!/usr/bin/env bash

gpu=$1
modelName=$2
dataset=$3
weights=$4
phase="predict"

export CUDA_VISIBLE_DEVICES=$gpu

python keras_models.py --gpu 0 --model_name ${modelName} --image_dir ${dataset} --phase ${phase} --weights ${weights}