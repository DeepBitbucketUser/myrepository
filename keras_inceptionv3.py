
from tensorflow.python.keras.applications.inception_v3 import InceptionV3
from tensorflow.python.keras.optimizers import SGD

import os


class DNNmodel(object):

    def __init__(self):

        self.save_period = 3
        self.freeze_first_n_layers = 249
        self.model_shape = 299

        self.use_model_shape = True

        self.train_split = 'train_split.txt'
        self.test_split = 'test_split.txt'
        self.predict_split = 'test_split.txt'

        self.batch_size = 10
        self.lr = 0.0003


    def get_name(self):
        return 'InceptionV3'

    def get_output_layer_name(self):
        return 'avg_pool'

    def get_data_split(self):
        return {
            'train': self.train_split,
            'test': self.test_split,
            'predict': self.predict_split
        }

    def get_generator(self):
        return None

    def get_base_model(self, include_top=False):
        return InceptionV3(weights='imagenet', include_top=include_top)


    def get_retrain_parameters(self):
        return {
            'steps_per_epoch': 1000,
            'epochs': 6,
            'validation_steps': 200
        }


    def get_optimizer(self):
        return SGD(lr=0.0001, momentum=0.9)


