from tensorflow.python.keras.applications.resnet50 import ResNet50
from tensorflow.python.keras.optimizers import SGD, Adam



class DNNmodel(object):

    def __init__(self):

        # split
        self.train_split = 'train_split.txt'
        self.test_split = 'test_split.txt'
        self.predict_split = 'test_split.txt'


        # input image shapes
        self.use_model_shape = True
        self.model_shape = 224
        self.min_shape = 197

        self.save_period = 2 # Interval (number of epochs) between checkpoints.
        self.freeze_first_n_layers = None
        self.batch_size = 20

        self.log_every_n_steps = 25


        self.lr = 0.003
        self.decay = 0.0


    def get_name(self):
        return 'ResNet50'

    def get_output_layer_name(self):
        return 'flatten_1'

    def get_data_split(self):
        return {
            'train': self.train_split,
            'test': self.test_split,
            'predict': self.predict_split
        }

    def get_generator(self):
        return None


    def get_base_model(self, include_top=False):
        return ResNet50(weights='imagenet', include_top=include_top)


    def get_retrain_parameters(self):
        return {
            'steps_per_epoch': 3500,
            'epochs': 40,
            'validation_steps': 850,
        }


    def get_optimizer(self):
        return Adam(
            lr = self.lr
        )
